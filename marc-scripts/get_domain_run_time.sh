###################################################
# Bash script to check if a domain is running     #
# The command xl sched-credit is used to retrieve #
# information.									  #
# Author Andrea Corna                             #
# andrea.corna@mail.polimi.it                     #
###################################################

#!/bin/bash

while true; do 
	OUTPUT=$(sudo xl sched-credit)
	TIMESTAMP=$(echo $(($(date +%s%N)/1000000)))
	echo $TIMESTAMP"-"$OUTPUT >> /home/marc/prova.log
	#sleep for 10 ms
	sleep 0.01
done